import { Link, Outlet } from "react-router-dom";

function Home(){
   return(
      <div>
         <h1>Home Page</h1>
         <Link to="/products">Products</Link>
         <Outlet />
      </div>
   )
}

export default Home