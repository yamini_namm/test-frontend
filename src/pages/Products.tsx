import { useState, useEffect } from "react";
import { useFetch } from "../hooks/useFetch";
import ProductsList from "../Components/ProductsList";
import Filter from "../Components/Filter";
const LIMIT = 8;

interface optionState {
  price: string;
  subscription: string;
  tags: string[];
  [key: string]: any;
}

const Products = () => {
  const [page, setPagination] = useState(1);
  const url = `/products?_page=${page}&_limit=${LIMIT}`;
  const { isLoading, response, error } = useFetch(url);
  const [filteredData, setFilteredData] = useState([]);
  const [options, setOptions] = useState<optionState>({
    price: "",
    subscription: "",
    tags: [],
  });

  useEffect(() => {
    setOptions({
      price: "",
      subscription: "",
      tags: [],
    });
    setFilteredData(response);
  }, [response]);

  const filterClickListener = (event: any, prop: string) => {
    let { price, subscription, tags } = options;
    if (prop === "tags") {
      tags = !event.target.checked
        ? options.tags.filter((item) => item !== event.target.value)
        : [...options.tags, event.target.value];
      setOptions((prevState) => ({ ...prevState, tags: tags }));
    } else {
      prop === "price"
        ? (price = event.target.value)
        : (subscription = event.target.value);
      setOptions((prevState) => ({ ...prevState, [prop]: event.target.value }));
    }
    let result = response;
    for (let key in options) {
      if (key === prop || options[key] || options[key].length) {
        if (key === "price") {
          result = result.filter((data: any) => data.price <= parseInt(price));
        } else if (key === "subscription") {
          result = result.filter(
            (data: any) =>
              data.subscription.toString() === subscription.toString()
          );
        } else if (key === "tags" && tags.length) {
          result = result.filter((data: any) =>
            data.tags.some((tag: any) => tags.includes(tag))
          );
        }
      }
    }
    setFilteredData(result);
  };

  const clearFilter = () => {
    setOptions({
      price: "",
      subscription: "",
      tags: [],
    });
    setFilteredData(response);
  };

  const updatePage = (page: number) => {
    setPagination(page);
  };

  if (isLoading || error) {
    return <div>NO DATA</div>;
  }
  return (
    <div className="container">
      <h1 className="heading">Products Page</h1>
      <div className="flex-container">
        <Filter
          settings={options}
          filterClickListener={filterClickListener}
          clearFilter={clearFilter}
        ></Filter>
        <ProductsList response={filteredData}></ProductsList>
      </div>
      <div className="pagination">
        <button disabled={page === 1} onClick={() => updatePage(page - 1)}>
          Prev
        </button>
        <span> Page {page} </span>
        <button
          disabled={response.length < LIMIT}
          onClick={() => updatePage(page + 1)}
        >
          Next
        </button>
      </div>
    </div>
  );
};

export default Products;
