import Card from "./Card";
import TableHeader from "./TableHeader";

interface ProductsProps {
  response: Array<Type>;
}

interface Type {
  id: number;
  title: string;
  price: number;
  tags: string[];
  subscription: boolean;
  subscription_discount: number;
}

const ProductsList = ({ response }: ProductsProps) => {
  if (!response.length) {
    return <>No data found</>;
  }
  return (
    <div className="products-list">
      <div className="sub-heading">Number of Products: {response.length}</div>
      <table className="table-main">
        <TableHeader></TableHeader>
        <tbody>
          {response.map((data: any) => (
            <Card
              key={data.title}
              title={data.title}
              price={data.price}
              subscription={data.subscription}
              discount={data.subscription_discount}
              tags={data.tags}
            ></Card>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ProductsList;
