import CheckboxGroup from './UI/CheckboxGroup';
import Dropdown from './UI/Dropdown';
import PriceRanger from './UI/PriceRanger';
const TAGS_ARR = ["Dog", "Cat", "Chews", "Formula", "Shampoo"];
const PRICE = {min: 0, max: 150};

interface filterProps {
  settings: {
    price: string;
    subscription: string;
    tags: string[];
  };
  filterClickListener: Function;
  clearFilter: Function;
}

const Filter = ({
  settings,
  filterClickListener,
  clearFilter,
}: filterProps) => {

  return (
    <div className="filter-container">
      <h4>Filter</h4>
      <Dropdown label="subscription" options={[true, false]} value={settings.subscription} onChangeFn={filterClickListener}></Dropdown>
      <PriceRanger label="price" options={PRICE} value={settings.price} onChangeFn={filterClickListener}></PriceRanger>
      <CheckboxGroup label="tags" options={TAGS_ARR} value={settings.tags} onChangeFn={filterClickListener}></CheckboxGroup>
      <div>
        <button onClick={() => clearFilter()}>Clear filter</button>
      </div>
    </div>
  );
};

export default Filter;
