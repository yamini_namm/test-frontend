interface dropdownProps {
   label: string;
   options: Array<string>;
   value: Array<string>;
   onChangeFn: Function;
 }
 
 const CheckboxGroup = ({ label, options, value, onChangeFn }: dropdownProps) => {
   return (
      <div>
        <p className="tags-heading">{label}</p>
        <ul className="list">
          {options.map((tag) => (
            <li key={tag}>
              <input
                type="checkbox"
                name="tags"
                value={tag}
                onChange={(event) => onChangeFn(event, label)}
                checked={value.indexOf(tag) !== -1}
              />
              <label>{tag}</label>
            </li>
          ))}
        </ul>
      </div>
   );
 };
 export default CheckboxGroup;
 