interface priceProps {
  label: string;
  options: {
    min: number;
    max: number;
  };
  value: string;
  onChangeFn: Function;
}

const PriceRanger = ({ label, options, value, onChangeFn }: priceProps) => {
  return (
    <div>
      <label>{label}:</label>
      <input
        type="range"
        min={options.min}
        max={options.max}
        onChange={(event) => onChangeFn(event, label)}
      />
      <span>{value}</span>
    </div>
  );
};
export default PriceRanger;
