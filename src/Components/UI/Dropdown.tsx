interface dropdownProps {
  label: string;
  options: Array<any>,
  value: string;
  onChangeFn: Function;
}

const Dropdown = ({ label, options, value, onChangeFn }: dropdownProps) => {
  return (
    <div className="block1">
      <label>{label}:</label>
      <select
        name={label}
        value={value}
        onChange={(event) => onChangeFn(event, label)}
      >
        <option value="" disabled hidden>
          Choose here
        </option>
        {options.map((opt)=>(
           <option key={opt} value={opt}>{opt.toString()}</option>
        ))}
      </select>
    </div>
  );
};
export default Dropdown;
