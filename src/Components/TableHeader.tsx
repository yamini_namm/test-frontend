const TableHeader = () => {
  return (
   <thead>
    <tr>
      <th className='table-header'>Title</th>
      <th className='table-header'>Tags</th>
      <th className='table-header'>Price</th>
      <th className='table-header'>Subscription</th>
      <th className='table-header'>Discount</th>
    </tr>
    </thead>
  );
};

export default TableHeader;
