import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import TableHeader from "../TableHeader";

beforeEach(() => {
  render(
    <table>
      <TableHeader></TableHeader>
    </table>
  );
});

test("TableHeader", async () => {
  expect(screen.getByText("Title")).toBeInTheDocument();
  expect(screen.getByText("Tags")).toBeInTheDocument();
  expect(screen.getByText("Price")).toBeInTheDocument();
  expect(screen.getByText("Subscription")).toBeInTheDocument();
  expect(screen.getByText("Discount")).toBeInTheDocument();
});
