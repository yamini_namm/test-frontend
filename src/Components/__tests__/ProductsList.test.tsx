import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import ProductsList from "../ProductsList";

const mockData= [
   {
      id: 1,
      title: 'title1',
      price: 24,
      subscription: true,
      subscription_discount: 10,
      tags: ['tag1','tag2']
   },
   {
      id: 2,
      title: 'title2',
      price: 24,
      subscription: false,
      subscription_discount: 0,
      tags: ['tag2']
   }
]

beforeEach(() => {
  render(
      <ProductsList response={mockData}></ProductsList>
  );
});

test("ProductsList", async () => {
  expect(screen.getByText("tag1,tag2")).toBeInTheDocument();
});

test("When response is empty array", async () => {
   render(
      <ProductsList response={[]}></ProductsList>
  );
  expect(screen.getByText("No data found")).toBeInTheDocument();
});
