import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import React from "react";
import Products from "../../pages/Products";
 
// mock config
const mockConfig = {
   isLoading: true, response: [], error: null
};

// this will mock complete file, we have provided mock implementation
// for useFetch function
jest.mock("../../hooks/useFetch", () => ({
   useFetch: () => mockConfig
}));

test("when api gives no data", async () => {
   const setStateMock = jest.fn();
   const useStateMock: any = (useState: any) => [useState, setStateMock];
   jest.spyOn(React, "useState").mockImplementation(useStateMock);
   jest.spyOn(React, "useEffect").mockImplementation((f) => f());

   render(<Products></Products>);
   expect(screen.getByText("NO DATA"));
 });