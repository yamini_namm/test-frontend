import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Card from "../Card";

beforeEach(() => {
  render(
    <table>
      <tbody>
      <Card
      title="title"
      price={24}
      subscription={true}
      discount={4}
      tags={["tag1", "tag2"]}
    ></Card>
    </tbody>
    </table>
  );
});

test("Card", async () => {
  expect(screen.getByText("title")).toBeInTheDocument();
  expect(screen.getByText("tag1,tag2")).toBeInTheDocument();
});
