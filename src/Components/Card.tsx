interface card {
  title: string;
  price: number;
  subscription: boolean;
  discount: number;
  tags: string[];
}
const Card = ({ title, price, subscription, discount, tags }: card) => {
  const tags_join = tags.join(",");
  return (
    <tr>
      <td className="table-cell">{title}</td>
      <td className="table-cell">{tags_join}</td>
      <td className="table-cell">{price}</td>
      <td className="table-cell">{subscription.toString()}</td>
      <td className="table-cell">{discount}</td>
    </tr>
  );
};

export default Card;
