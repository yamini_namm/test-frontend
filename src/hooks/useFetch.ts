import { useState, useEffect } from "react";
import axios from "axios";

export const useFetch = (url: string) => {
   const [isLoading, setIsLoading] = useState(false);
   const [response, setResponse] = useState([]);
   const [error, setError] = useState(null);
 
   useEffect(() => {
     setIsLoading(true);
     const fetchData = async () => {
       try {
         const resp = await axios.get(url);
         const data = await resp?.data;
         setResponse(data);
         setIsLoading(false);
       } catch (error: any) {
         setError(error);
         setIsLoading(false);
       }
     };
     fetchData();
   }, [url]);
 
   return { isLoading, response, error };
 };